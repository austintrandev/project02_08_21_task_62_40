package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.*;

@RestController
public class MainController {
	@CrossOrigin
	@GetMapping("/country/regions")
	public ArrayList<Region> getRegionList(@RequestParam(value = "countryCode", defaultValue = "0") String countryCode) {

		ArrayList<Region> vVietNamRegion = new ArrayList<Region>();
		Region vVNRegion1 = new Region("VNR101", "Hà Nội");
		Region vVNRegion2 = new Region("VNR102", "HCM");
		Region vVNRegion3 = new Region("VNR103", "Đà Nẵng");
		vVietNamRegion.add(vVNRegion1);
		vVietNamRegion.add(vVNRegion2);
		vVietNamRegion.add(vVNRegion3);

		ArrayList<Region> vJaPanRegion = new ArrayList<Region>();
		Region vJPRegion1 = new Region("JPR101", "Tokyo");
		Region vJPRegion2 = new Region("JPR102", "Osaka");
		Region vJPRegion3 = new Region("JPR103", "Kobe");
		vJaPanRegion.add(vJPRegion1);
		vJaPanRegion.add(vJPRegion2);
		vJaPanRegion.add(vJPRegion3);

		ArrayList<Region> vKoreaRegion = new ArrayList<Region>();
		Region vKRRegion1 = new Region("KRR101", "Seoul");
		Region vKRRegion2 = new Region("KRR102", "Busan");
		Region vKRRegion3 = new Region("KRR103", "Jeju");
		vKoreaRegion.add(vKRRegion1);
		vKoreaRegion.add(vKRRegion2);
		vKoreaRegion.add(vKRRegion3);

		Country vVietNam = new Country("VN101", "Việt Nam", vVietNamRegion);
		Country vJaPan = new Country("JP111", "JaPan", vJaPanRegion);
		Country vKorea = new Country("KR123", "Korea", vKoreaRegion);

		ArrayList<Country> vCountryList = new ArrayList<Country>();
		vCountryList.add(vVietNam);
		vCountryList.add(vJaPan);
		vCountryList.add(vKorea);
		return filterRegionByCountry(vCountryList, countryCode);
		
	}
	public ArrayList<Region> filterRegionByCountry(ArrayList<Country> countryList, String countryCode) {
		for (Country country : countryList) {
				if (country.getCountryCode().equals(countryCode)) {
					return country.getRegions();
				}
			}
		return null;
	}
	
	@CrossOrigin
	@GetMapping("/country")
	public ArrayList<Country> getCountryList() {

		ArrayList<Region> vVietNamRegion = new ArrayList<Region>();
		Region vVNRegion1 = new Region("VNR101", "Hà Nội");
		Region vVNRegion2 = new Region("VNR102", "HCM");
		Region vVNRegion3 = new Region("VNR103", "Đà Nẵng");
		vVietNamRegion.add(vVNRegion1);
		vVietNamRegion.add(vVNRegion2);
		vVietNamRegion.add(vVNRegion3);

		ArrayList<Region> vJaPanRegion = new ArrayList<Region>();
		Region vJPRegion1 = new Region("JPR101", "Tokyo");
		Region vJPRegion2 = new Region("JPR102", "Osaka");
		Region vJPRegion3 = new Region("JPR103", "Kobe");
		vJaPanRegion.add(vJPRegion1);
		vJaPanRegion.add(vJPRegion2);
		vJaPanRegion.add(vJPRegion3);

		ArrayList<Region> vKoreaRegion = new ArrayList<Region>();
		Region vKRRegion1 = new Region("KRR101", "Seoul");
		Region vKRRegion2 = new Region("KRR102", "Busan");
		Region vKRRegion3 = new Region("KRR103", "Jeju");
		vKoreaRegion.add(vKRRegion1);
		vKoreaRegion.add(vKRRegion2);
		vKoreaRegion.add(vKRRegion3);

		Country vVietNam = new Country("VN101", "Việt Nam", vVietNamRegion);
		Country vJaPan = new Country("JP111", "JaPan", vJaPanRegion);
		Country vKorea = new Country("KR123", "Korea", vKoreaRegion);

		ArrayList<Country> vCountryList = new ArrayList<Country>();
		vCountryList.add(vVietNam);
		vCountryList.add(vJaPan);
		vCountryList.add(vKorea);
		
		return vCountryList;
		
	}
	
}
