package com.example.model;

import java.util.ArrayList;

public class Country {
	private String CountryCode;

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	private String CountryName;
	ArrayList<Region> regions = new ArrayList();

	public Country() {

	}

	public Country(String CountryCode, String CountryName, ArrayList<Region> regions) {
		this.CountryCode = CountryCode;
		this.CountryName = CountryName;
		this.regions = regions;
	}

	/**
	 * @return the regions
	 */
	public ArrayList<Region> getRegions() {
		return regions;
	}

	/**
	 * @param regions the regions to set
	 */
	public void setRegions(ArrayList<Region> regions) {
		this.regions = regions;
	}

}
