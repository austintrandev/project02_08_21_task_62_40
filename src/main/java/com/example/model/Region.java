package com.example.model;

import java.util.ArrayList;

public class Region {
	protected String regionCode;

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	protected String regionName;

	public Region(String regionCode, String regionName) {
		this.regionCode = regionCode;
		this.regionName = regionName;
	}

}
